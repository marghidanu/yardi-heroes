#!/bin/bash

# TODO: Validate parameters

BASE='http://www.superherodb.com'

cat $1 | egrep -v '^"' | while read line
do
    URL=`echo $line | cut -d , -f1`
    HERO_NAME=`echo $line | cut -d , -f2`
    HERO_FULL_NAME=`echo $line | cut -d , -f3`

    # NOTE: The trailing backslash in the url is very important!

    echo "${HERO_FULL_NAME}"
    curl -s "${BASE}${URL}${2}/" -o "${HERO_NAME}.html"
done
