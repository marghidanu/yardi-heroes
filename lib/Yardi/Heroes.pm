package Yardi::Heroes;

use MooseX::App;

our $VERSION = '1.0';

app_namespace( 'Yardi::Heroes::Commands' );

1;
