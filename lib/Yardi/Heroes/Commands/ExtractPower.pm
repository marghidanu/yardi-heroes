package Yardi::Heroes::Commands::ExtractPower;

use MooseX::App::Command;

use File::Slurp;

use Mojo::DOM;
use Mojo::Util qw( squish );
use Mojo::JSON qw( encode_json );

option 'file' => (
    is => 'rw',
    isa => 'Str',
    required => 1,
    cmd_aliases => [ qw( f ) ]
);

sub run {
    my $self = shift();

    my $content = read_file( $self->file() );
    my $dom = Mojo::DOM->new( $content );

    my $result = {
        name => $dom->at( 'h1' )->text(),
        description => squish( $dom->at( 'div[class~=content]' )->content() ),
        characters => [],
    };

    $dom->find( 'img[class~=portrait]' )->each( sub {
        push( @{ $result->{characters} }, $_->attr( 'alt' ) );
    } );

    printf( "%s\n", encode_json( $result ) );
}

1;
