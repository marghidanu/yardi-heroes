package Yardi::Heroes::Commands::UtilLinksExtractor;

use MooseX::App::Command;

use File::Slurp;

use Mojo::DOM;
use Mojo::Util qw( decamelize dumper squish );

use String::Util qw( no_space );

option 'file' => (
    is => 'rw',
    isa => 'Str',
    required => 1,
    cmd_aliases => [ qw( f ) ]
);

sub run {
    my $self = shift();

    my $content = read_file( $self->file() );
    my $dom = Mojo::DOM->new( $content );

    $dom->find( 'a' )->each( sub {
        my $element = shift();

        printf( "%s,%s,%s\n", $element->attr( 'href' ), no_space( decamelize( $element->text() ) ), $element->text() );
    } );
}

1;
