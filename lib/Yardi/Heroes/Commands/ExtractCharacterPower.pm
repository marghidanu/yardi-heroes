package Yardi::Heroes::Commands::ExtractCharacterPower;

use MooseX::App::Command;

use File::Slurp;

use Mojo::DOM;
use Mojo::JSON qw( encode_json );

option 'file' => (
    is => 'rw',
    isa => 'Str',
    required => 1,
    cmd_aliases => [ qw( f ) ]
);

sub run {
    my $self = shift();

    my $content = read_file( $self->file() );
    my $dom = Mojo::DOM->new( $content );

    my $result = {
        character => $dom->at( 'h1' )->text(),
        name => $dom->at( 'h2' )->text(),
    };

    $dom->find( 'div[class="cblock linkunderline"]' )->each( sub {
        my $block = shift();

        $block->find( 'h3' )->each( sub {
            my $category = shift();

            my @elements = split( /\s/, $category->text() );
            my $name = lc( pop( @elements ) );

            return
                unless( defined( $category->next() ) );

            $category->next()->find( 'h4' )->each( sub {
                my $title = shift();

                my $key = $title->text();
                my $value = defined( $title->next() ) ? $title->next()->text() : undef;

                $result->{ $name }->{ $key } = $value;
            } );
        } );
    } );

    printf( "%s\n", encode_json( $result ) );
}

1;
