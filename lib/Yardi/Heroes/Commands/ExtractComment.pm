package Yardi::Heroes::Commands::ExtractComment;

use MooseX::App::Command;

use File::Slurp;

use Mojo::DOM;
use Mojo::JSON qw( encode_json );

option 'file' => (
    is => 'rw',
    isa => 'Str',
    required => 1,
    cmd_aliases => [ qw( f ) ]
);

sub run {
	my $self = shift();

	my $content = read_file( $self->file() );
	my $dom = Mojo::DOM->new( $content );

	my $result = {
		character => $dom->at( 'h1' )->text(),
		name => $dom->at( 'h2' )->text(),
	};

	$result->{comments} =
		$dom->find( 'div[class=commBody]' )->map( sub {
			my $element = shift();

			return {
				author => $element->at( 'div[class=commInfo]' )->at( 'a' )->text(),
				content => $element->at( 'div[class=commContent]' )->text(),
				date => $element->at( 'span[class=commTime]' )->text(),
			};
		} )->to_array();

	printf( "%s\n", encode_json( $result ) );
}

1;
