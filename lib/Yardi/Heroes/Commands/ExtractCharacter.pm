package Yardi::Heroes::Commands::ExtractCharacter;

use MooseX::App::Command;

use File::Slurp;

use Mojo::DOM;
use Mojo::JSON qw( encode_json );
use Mojo::Util qw( decamelize dumper squish );

use String::Util qw( no_space );

option 'file' => (
    is => 'rw',
    isa => 'Str',
    required => 1,
    cmd_aliases => [ qw( f ) ]
);

sub run {
    my $self = shift();

    my $content = read_file( $self->file() );
    my $dom = Mojo::DOM->new( $content );

    my $result = {
    	character => $dom->at( 'h1' )->text(),
    	name => $dom->at( 'h2' )->text(),
    };

    # --- Powerstats
    $result->{powerstats} = undef;
    if( $content !~ /No data has been entered yet/ ) {
    	$dom->at( 'h3' )->next()->children()->each( sub {
    		my $element = shift();

    		my $value = int( $element->at( 'div[class~=gridbarvalue]' )->text() );

    		$result->{powerstats}->{ no_space( decamelize( $element->at( 'div[class~=gridbarlabel]' )->text() ) ) } = $value;
    	} );
    }

    # --- Properties
    $dom->find( 'tr' )->each( sub {
    	my $element = shift();

    	my $value = $element->at( 'td' )->next()->text() eq '-' ?
    		undef :
    		$element->at( 'td' )->next()->text();

    	$result->{ no_space( decamelize( $element->at( 'td' )->text() ) ) } = $value;
    } );


    printf( "%s\n", encode_json( $result ) );
}

1;
