#!/usr/bin/env python

import sys
import nltk.classify.util
from nltk.classify import NaiveBayesClassifier

f1 = open('neg_comments', 'r')
f2 = open('pos_comments', 'r')

negcomms = f1.read().split('\n')[:-1]
poscomms = f2.read().split('\n')[:-1]

f1.close()
f2.close()
 
def word_feats(words):
#    return dict([(word, True) for word in words.split()])
    return dict([(word, True) for word in nltk.word_tokenize(words)])
 
negfeats = [(word_feats(sen), 'neg') for sen in negcomms]
posfeats = [(word_feats(sen.decode('utf-8')), 'pos') for sen in poscomms]


negcutoff = len(negfeats)*3/4
poscutoff = len(posfeats)*3/4
 
trainfeats = negfeats[:negcutoff] + posfeats[:poscutoff]
testfeats = negfeats[negcutoff:] + posfeats[poscutoff:]
print 'train on %d instances, test on %d instances' % (len(trainfeats), len(testfeats))
 
classifier = NaiveBayesClassifier.train(trainfeats)
print 'accuracy:', nltk.classify.util.accuracy(classifier, testfeats)
classifier.show_most_informative_features()
