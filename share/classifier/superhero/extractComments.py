from pymongo import MongoClient

client = MongoClient()
db = client.yss
col = db.comments
f = open("comments.txt","w")
comments = [x["comments"] for x in col.find({}, {"_id":False, "comments.content": True})]
for i in comments:
	if i != []:
		for j in i:
			try:
				f.write(j["content"]+ "\n")
			except:
				continue
f.close()
