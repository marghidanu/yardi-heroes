/powers/ability-shift/60-063/,ability_shift,Ability Shift
/powers/absorption/60-064/,absorption,Absorption
/powers/accuracy/60-084/,accuracy,Accuracy
/powers/adaptation/60-065/,adaptation,Adaptation
/powers/aerokinesis/60-008/,aerokinesis,Aerokinesis
/powers/agility/60-107/,agility,Agility
/powers/animal-attributes/60-001/,animal_attributes,Animal Attributes
/powers/animal-oriented-powers/60-002/,animal_oriented_powers,Animal Oriented Powers
/powers/animation/60-036/,animation,Animation
/powers/anti-gravity/60-149/,anti-_gravity,Anti-Gravity
/powers/apotheosis/60-066/,apotheosis,Apotheosis
/powers/astral-projection/60-037/,astral_projection,Astral Projection
/powers/astral-trap/60-038/,astral_trap,Astral Trap
/powers/astral-travel/60-125/,astral_travel,Astral Travel
/powers/atmokinesis/60-009/,atmokinesis,Atmokinesis
/powers/audiokinesis/60-010/,audiokinesis,Audiokinesis
/powers/banish/60-039/,banish,Banish
/powers/biokinesis/60-011/,biokinesis,Biokinesis
/powers/bullet-time/60-067/,bullet_time,Bullet Time
/powers/camouflage/60-120/,camouflage,Camouflage
/powers/changing-armor/60-068/,changing_armor,Changing Armor
/powers/chlorokinesis/60-012/,chlorokinesis,Chlorokinesis
/powers/chronokinesis/60-013/,chronokinesis,Chronokinesis
/powers/clairvoyance/60-040/,clairvoyance,Clairvoyance
/powers/cloaking/60-181/,cloaking,Cloaking
/powers/cold-resistance/60-152/,cold_resistance,Cold Resistance
/powers/cross-dimensional-awareness/60-041/,cross-_dimensional_awareness,Cross-Dimensional Awareness
/powers/cross-dimensional-travel/60-157/,cross-_dimensional_travel,Cross-Dimensional Travel
/powers/cryokinesis/60-014/,cryokinesis,Cryokinesis
/powers/danger-sense/60-042/,danger_sense,Danger Sense
/powers/darkforce-manipulation/60-150/,darkforce_manipulation,Darkforce Manipulation
/powers/death-touch/60-172/,death_touch,Death Touch
/powers/density-control/60-069/,density_control,Density Control
/powers/dexterity/60-109/,dexterity,Dexterity
/powers/duplication/60-070/,duplication,Duplication
/powers/durability/60-111/,durability,Durability
/powers/echokinesis/60-015/,echokinesis,Echokinesis
/powers/elasticity/60-071/,elasticity,Elasticity
/powers/electrical-transport/60-087/,electrical_transport,Electrical Transport
/powers/electrokinesis/60-016/,electrokinesis,Electrokinesis
/powers/elemental-transmogrification/60-003/,elemental_transmogrification,Elemental Transmogrification
/powers/empathy/60-057/,empathy,Empathy
/powers/endurance/60-112/,endurance,Endurance
/powers/energy-absorption/60-110/,energy_absorption,Energy Absorption
/powers/energy-armor/60-028/,energy_armor,Energy Armor
/powers/energy-beams/60-029/,energy_beams,Energy Beams
/powers/energy-blasts/60-030/,energy_blasts,Energy Blasts
/powers/energy-constructs/60-161/,energy_constructs,Energy Constructs
/powers/energy-manipulation/60-123/,energy_manipulation,Energy Manipulation
/powers/energy-resistance/60-136/,energy_resistance,Energy Resistance
/powers/enhanced-hearing/60-100/,enhanced_hearing,Enhanced Hearing
/powers/enhanced-memory/60-156/,enhanced_memory,Enhanced Memory
/powers/enhanced-senses/60-072/,enhanced_senses,Enhanced Senses
/powers/enhanced-smell/60-160/,enhanced_smell,Enhanced Smell
/powers/enhanced-touch/60-173/,enhanced_touch,Enhanced Touch
/powers/entropy-projection/60-031/,entropy_projection,Entropy Projection
/powers/fire-resistance/60-139/,fire_resistance,Fire Resistance
/powers/flight/60-088/,flight,Flight
/powers/force-fields/60-032/,force_fields,Force Fields
/powers/geokinesis/60-122/,geokinesis,Geokinesis
/powers/gliding/60-089/,gliding,Gliding
/powers/gravitokinesis/60-017/,gravitokinesis,Gravitokinesis
/powers/grim-reaping/60-073/,grim_reaping,Grim Reaping
/powers/healing-factor/60-074/,healing_factor,Healing Factor
/powers/heat-generation/60-177/,heat_generation,Heat Generation
/powers/heat-resistance/60-142/,heat_resistance,Heat Resistance
/powers/human-physical-perfection/60-140/,humanphysicalperfection,Human physical perfection
/powers/hydrokinesis/60-018/,hydrokinesis,Hydrokinesis
/powers/hyperkinesis/60-178/,hyperkinesis,Hyperkinesis
/powers/hypnokinesis/60-019/,hypnokinesis,Hypnokinesis
/powers/illumination/60-145/,illumination,Illumination
/powers/illusions/60-044/,illusions,Illusions
/powers/immortality/60-075/,immortality,Immortality
/powers/insanity/60-058/,insanity,Insanity
/powers/intangibility/60-113/,intangibility,Intangibility
/powers/intelligence/60-059/,intelligence,Intelligence
/powers/intuitive-aptitude/60-154/,intuitiveaptitude,Intuitive aptitude
/powers/invisibility/60-004/,invisibility,Invisibility
/powers/invulnerability/60-076/,invulnerability,Invulnerability
/powers/jump/60-093/,jump,Jump
/powers/lantern-power-ring/60-137/,lantern_power_ring,Lantern Power Ring
/powers/latent-abilities/60-045/,latent_abilities,Latent Abilities
/powers/levitation/60-090/,levitation,Levitation
/powers/longevity/60-141/,longevity,Longevity
/powers/magic/60-046/,magic,Magic
/powers/magic-resistance/60-133/,magic_resistance,Magic Resistance
/powers/magnetokinesis/60-020/,magnetokinesis,Magnetokinesis
/powers/matter-absorption/60-166/,matter_absorption,Matter Absorption
/powers/melting/60-155/,melting,Melting
/powers/mind-blast/60-047/,mind_blast,Mind Blast
/powers/mind-control/60-048/,mind_control,Mind Control
/powers/mind-control-resistance/60-116/,mind_control_resistance,Mind Control Resistance
/powers/molecular-combustion/60-077/,molecular_combustion,Molecular Combustion
/powers/molecular-dissipation/60-079/,molecular_dissipation,Molecular Dissipation
/powers/molecular-immobilization/60-078/,molecular_immobilization,Molecular Immobilization
/powers/molecular-manipulation/60-126/,molecular_manipulation,Molecular Manipulation
/powers/natural-armor/60-080/,natural_armor,Natural Armor
/powers/natural-weapons/60-081/,natural_weapons,Natural Weapons
/powers/nova-force/60-163/,nova_force,Nova Force
/powers/omnilingualism/60-049/,omnilingualism,Omnilingualism
/powers/omnipotence/60-127/,omnipotence,Omnipotence
/powers/omnitrix/60-165/,omnitrix,Omnitrix
/powers/orbing/60-091/,orbing,Orbing
/powers/phasing/60-114/,phasing,Phasing
/powers/photographic-reflexes/60-082/,photographic_reflexes,Photographic Reflexes
/powers/photokinesis/60-021/,photokinesis,Photokinesis
/powers/physical-anomaly/60-005/,physical_anomaly,Physical Anomaly
/powers/portal-creation/60-092/,portal_creation,Portal Creation
/powers/possession/60-050/,possession,Possession
/powers/power-absorption/60-033/,power_absorption,Power Absorption
/powers/power-augmentation/60-132/,power_augmentation,Power Augmentation
/powers/power-cosmic/60-128/,power_cosmic,Power Cosmic
/powers/power-nullifier/60-129/,power_nullifier,Power Nullifier
/powers/power-sense/60-158/,power_sense,Power Sense
/powers/power-suit/60-159/,power_suit,Power Suit
/powers/precognition/60-051/,precognition,Precognition
/powers/probability-manipulation/60-052/,probability_manipulation,Probability Manipulation
/powers/projection/60-053/,projection,Projection
/powers/psionic-powers/60-124/,psionic_powers,Psionic Powers
/powers/psychokinesis/60-022/,psychokinesis,Psychokinesis
/powers/pyrokinesis/60-023/,pyrokinesis,Pyrokinesis
/powers/qwardian-power-ring/60-162/,qwardian_power_ring,Qwardian Power Ring
/powers/radar-sense/60-174/,radar_sense,Radar Sense
/powers/radiation-absorption/60-115/,radiation_absorption,Radiation Absorption
/powers/radiation-control/60-054/,radiation_control,Radiation Control
/powers/radiation-immunity/60-143/,radiation_immunity,Radiation Immunity
/powers/reality-warping/60-055/,reality_warping,Reality Warping
/powers/reflexes/60-108/,reflexes,Reflexes
/powers/regeneration/60-144/,regeneration,Regeneration
/powers/resurrection/60-135/,resurrection,Resurrection
/powers/seismic-power/60-147/,seismic_power,Seismic Power
/powers/self-sustenance/60-138/,self-_sustenance,Self-Sustenance
/powers/separation/60-083/,separation,Separation
/powers/shapeshifting/60-006/,shapeshifting,Shapeshifting
/powers/size-changing/60-007/,size_changing,Size Changing
/powers/sonar/60-117/,sonar,Sonar
/powers/sonic-scream/60-034/,sonic_scream,Sonic Scream
/powers/spatial-awareness/60-179/,spatial_awareness,Spatial Awareness
/powers/stamina/60-106/,stamina,Stamina
/powers/stealth/60-176/,stealth,Stealth
/powers/sub-mariner/60-171/,sub-_mariner,Sub-Mariner
/powers/substance-secretion/60-153/,substance_secretion,Substance Secretion
/powers/summoning/60-056/,summoning,Summoning
/powers/super-breath/60-101/,super_breath,Super Breath
/powers/super-speed/60-094/,super_speed,Super Speed
/powers/super-strength/60-085/,super_strength,Super Strength
/powers/symbiote-costume/60-118/,symbiote_costume,Symbiote Costume
/powers/technopathcyberpath/60-061/,technopath/_cyberpath,Technopath/Cyberpath
/powers/telekinesis/60-099/,telekinesis,Telekinesis
/powers/telepathy/60-062/,telepathy,Telepathy
/powers/telepathy-resistance/60-130/,telepathy_resistance,Telepathy Resistance
/powers/teleportation/60-095/,teleportation,Teleportation
/powers/terrakinesis/60-024/,terrakinesis,Terrakinesis
/powers/thermokinesis/60-025/,thermokinesis,Thermokinesis
/powers/thirstokinesis/60-164/,thirstokinesis,Thirstokinesis
/powers/time-travel/60-097/,time_travel,Time Travel
/powers/timeframe-control/60-096/,timeframe_control,Timeframe Control
/powers/toxikinesis/60-134/,toxikinesis,Toxikinesis
/powers/toxin-and-disease-resistance/60-131/,toxinand_disease_resistance,Toxin and Disease Resistance
/powers/umbrakinesis/60-026/,umbrakinesis,Umbrakinesis
/powers/underwater-breathing/60-146/,underwaterbreathing,Underwater breathing
/powers/vaporising-beams/60-035/,vaporising_beams,Vaporising Beams
/powers/vision---cryo/60-180/,vision-_cryo,Vision - Cryo
/powers/vision---heat/60-103/,vision-_heat,Vision - Heat
/powers/vision---infrared/60-175/,vision-_infrared,Vision - Infrared
/powers/vision---microscopic/60-105/,vision-_microscopic,Vision - Microscopic
/powers/vision---night/60-119/,vision-_night,Vision - Night
/powers/vision---telescopic/60-104/,vision-_telescopic,Vision - Telescopic
/powers/vision---thermal/60-121/,vision-_thermal,Vision - Thermal
/powers/vision---x-ray/60-102/,vision-_x-_ray,Vision - X-Ray
/powers/vitakinesis/60-027/,vitakinesis,Vitakinesis
/powers/wallcrawling/60-098/,wallcrawling,Wallcrawling
/powers/weapon-based-powers/60-086/,weapon-based_powers,Weapon-based Powers
/powers/weapons-master/60-167/,weapons_master,Weapons Master
/powers/web-creation/60-170/,web_creation,Web Creation
/powers/wishing/60-060/,wishing,Wishing
