var cursor = db.processed.find();

cursor.forEach( function( record ) {
	var sum = length = 0;
	for( var index in record.comments ) {
		if( typeof record.comments[ index ].content != 'number' )
			continue;

		sum += record.comments[ index ].content;
		length++;
	}

	var character = db.characters.findOne( { character: record.character } );

	character.sentiment = ( length > 0 ) ? sum/length : null;
	print( character.character + ': ' + character.sentiment );

	db.characters.save( character );
} );
