var cursor = db.characters.find( { powerstats: { $ne: null } }, { character : 1, powerstats: 1,  } );

cursor.forEach( function( record ) {
	var sum = length = 0;
	for( var key in record.powerstats ) {
		sum += record.powerstats[ key ];
		length++
	}

	print( record.character + ': ' + Math.round( sum/length ) );
	record.score = Math.round( sum/length );
	db.characters.save( record );
} );
