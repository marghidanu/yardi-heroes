var cursor = db.characters.find( { powerstats: { $ne: null } } );

cursor.forEach( function( record ) {
	var sum = length = 0;
	for( var key in record.powerstats ) {
		sum += record.powerstats[ key ];
		length++;
	}

	record.score = ( length > 0 ) ? sum/length : null;
	print( record.character + ' : ' + record.score );
	db.characters.save( record );
} );
