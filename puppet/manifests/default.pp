exec { "apt-update":
    command => "/usr/bin/apt-get update"
}

Exec["apt-update"] -> Package <| |>

package {
    "curl":
        ensure => "installed";

    "jq":
        ensure => "installed";

    "python-virtualenv":
	ensure => "installed";

    "mongodb":
        ensure => "installed"
}
